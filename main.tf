resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name               = "benchmark"
  description        = "Kubernetes cluster for benchmark"
  cluster_ipv4_range = "10.115.0.0/16"
  service_ipv4_range = "10.99.0.0/16"
  network_id = "enpsmul67gdnijm94f9p"

  master {
    version = "1.18"
    zonal {
      zone      = "ru-central1-a"
      subnet_id = "e9bm2fq1bp2l09ljtkv6"
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = false
    }
  }

  service_account_id      = "ajesbumgchoaei4188j0"
  node_service_account_id = "ajellpdg034b0ehhf7e9"

  labels = {
    env = "benchmark"
  }

  release_channel         = "REGULAR"
  network_policy_provider = "CALICO"

}

resource "yandex_kubernetes_node_group" "zone-a" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "zone-a"
  description = "description"
  version     = "1.18"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = false
      subnet_ids         = ["e9bm2fq1bp2l09ljtkv6"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }

  node_taints = ["zone=a:NoExecute"]
}

resource "yandex_kubernetes_node_group" "zone-b" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "zone-b"
  description = "description"
  version     = "1.18"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = false
      subnet_ids         = ["e2l2rojr1cc3ddvle9m9"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-b"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }

  node_taints = ["zone=b:NoExecute"]
}

resource "yandex_kubernetes_node_group" "zone-c" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "zone-c"
  description = "description"
  version     = "1.18"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = false
      subnet_ids         = ["b0ce669hcpmhmu7n5vbd"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-c"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }

  node_taints = ["zone=c:NoExecute"]
}

resource "yandex_kubernetes_node_group" "common" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "common"
  description = "description"
  version     = "1.18"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = false
      subnet_ids         = ["b0ce669hcpmhmu7n5vbd"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-c"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }

}