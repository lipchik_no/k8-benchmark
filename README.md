Тестирование времени получения ответа от различных зон в Yandex Cloud

1) Добавить provider.tf для работы с яндексом
2) Отредактировать id в соответствии со своим окружением
3) Применить тераформ создав кластер кубернетес с нодами
4) применить манифесты из папки k8
   * kubectl apply -f k8/postgresql.yaml
   * kubectl apply -f k8/webservers.yaml
   * kubectl apply -f k8/grafana.yaml
   * kubectl apply -f k8/pinger.yaml #После того как поднялись webservers и postgres
5) Настроить datasorce grafana на работу с postgres
6) Импортировать дашбоард в графану grafana/pinger-dashboard.json
